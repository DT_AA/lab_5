import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;

import java.nio.charset.StandardCharsets;

public class ConsumerAccept {
    public static void main(String[] args) throws Exception {
        Channel channel = Service.getChannel();
        DeliverCallback rejectCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            System.out.println(message);
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
        };
        channel.queueDeclare(Producer.QUEUE_NAME, false, false, false, null);
        channel.basicConsume(Producer.QUEUE_NAME, false, rejectCallback, consumerTag -> { });
    }
}
