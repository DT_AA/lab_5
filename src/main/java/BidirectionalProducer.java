import com.rabbitmq.client.Channel;

public class BidirectionalProducer {
    public static final String QUEUE_NAME = "Bidirectional";
    public static final String REVERSE_QUEUE_NAME = "Bidirectional_reverse";
    public static void main(String[] args) throws Exception {
        Channel channel = Service.getChannel();
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        channel.queueDeclare(REVERSE_QUEUE_NAME, false, false, false, null);
        channel.basicPublish("", QUEUE_NAME, null, "msg1".getBytes());

        channel.basicConsume(REVERSE_QUEUE_NAME, true, Service.printCallback, consumerTag -> { });
    }
}
