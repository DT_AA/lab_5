import com.rabbitmq.client.Channel;

public class Publisher {
    public static final String EXCHANGE_NAME = "publisher_subscriber";
    public static void main(String[] args) throws Exception {
        Channel channel = Service.getChannel();
        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        for (int i = 0; i < 10; i++) {
            String msg = "msg" + i;
            channel.basicPublish(EXCHANGE_NAME, "", null, msg.getBytes());
        }
    }
}
