import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class Service {

    public static DeliverCallback printCallback = (consumerTag, delivery) -> {
        String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
        System.out.println(message);
    };

    public static Channel getChannel() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        return connection.createChannel();
    }

    public static Map<String, Object> getMaxLengthParams() {
        Map<String, Object> args = new HashMap<>();
        args.put("x-max-length", 10);
        return args;
    }
    public static Map<String, Object> getMaxLengthRejectPublishParams() {
        Map<String, Object> args = getMaxLengthParams();
        args.put("x-overflow", "reject-publish");
        return args;
    }

}
