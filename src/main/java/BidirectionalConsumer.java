import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;

import java.nio.charset.StandardCharsets;

public class BidirectionalConsumer {
    public static void main(String[] args) throws Exception {
        Channel channel = Service.getChannel();
        channel.queueDeclare(BidirectionalProducer.QUEUE_NAME, false, false, false, null);
        channel.queueDeclare(BidirectionalProducer.REVERSE_QUEUE_NAME, false, false, false, null);
        DeliverCallback reverseCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            message = "hello " + message;
            channel.basicPublish("", BidirectionalProducer.REVERSE_QUEUE_NAME, null, message.getBytes());
        };
        channel.basicConsume(BidirectionalProducer.QUEUE_NAME, true, reverseCallback, consumerTag -> { });
    }
}
