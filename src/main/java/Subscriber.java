import com.rabbitmq.client.Channel;

public class Subscriber {
    public static void main(String[] args) throws Exception {
        Channel channel = Service.getChannel();
        channel.exchangeDeclare(Publisher.EXCHANGE_NAME, "fanout");
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, Publisher.EXCHANGE_NAME, "");
        channel.basicConsume(queueName, true, Service.printCallback, consumerTag -> { });
    }
}
