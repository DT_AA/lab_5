import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;

public class Producer {
    public static final String QUEUE_NAME = "manual_ack";
    public static void main(String[] args) throws Exception {
        Channel channel = Service.getChannel();
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        for (int i = 0; i < 10; i++) {
            String msg = "msg" + i;
            AMQP.BasicProperties bp = new AMQP.BasicProperties.Builder().expiration("10000").build();
            channel.basicPublish("", QUEUE_NAME, bp, msg.getBytes());
        }
    }
}
