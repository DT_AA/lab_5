import com.rabbitmq.client.Channel;

public class Consumer {
    public static void main(String[] args) throws Exception {
        Channel channel = Service.getChannel();
        channel.queueDeclare(Producer.QUEUE_NAME, false, false, false, null);
        channel.basicConsume(Producer.QUEUE_NAME, true, Service.printCallback, consumerTag -> { });
    }
}
